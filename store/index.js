

import Vuex from 'vuex'

const newstore = function(){
return new Vuex.Store({
    state:{},
    mutations:{},
    getters:{},
    actions:{}
  })
}

const createStore = () => {
  return new Vuex.Store({
    state: {
      
      shader:'dark',
      forgetter:'',
      counter: 0,
      nums:[
      1,2,3,4,5,6
    ]
    },
    mutations: {  //kind of setters java concept

      increment (state) {
        state.counter++;
      },
      hulk(state)
      {
        state.shader="green"
      }
    },
    getters:{   //kind of getters :P java concept
      get_hulk(state)
      {
        return  state.shader.length;
      },
      get_hulk_bulk(state,getters)
      {
        return getters.get_hulk*2;
      }
    },
    actions:{   //conditional mutations 
                // outsiders components will dispatch them via methods and conditional mutation would happend
     hulk(context)
     {
       
       context.commit('increment')
     }
     
    }
  })
}

export default createStore